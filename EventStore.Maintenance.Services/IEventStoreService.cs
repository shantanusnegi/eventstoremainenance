﻿namespace EventStore.Maintenance.Services
{
    public interface IEventStoreService
    {
        Task ScavengeAsync();
        Task SetStreamMaxAgeAsync(string stream, uint maxAgeInDays);
        Task CreateStreamNamesProjectionAsync(string projectionName, string streamsPrefix);
        Task<IEnumerable<string>> GetStreamNamesFromProjectionAsync(string projectionName);
    }
}
