﻿using EventStore.ClientAPI;
using EventStore.ClientAPI.Common.Log;
using EventStore.ClientAPI.Projections;
using EventStore.ClientAPI.SystemData;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;

namespace EventStore.Maintenance.Services
{
    public class EventStoreService : IEventStoreService
    {
        private readonly EventStoreSettings _settings;
        private readonly ConnectionSettingsBuilder _connectionSettings;
        private readonly ILogger<EventStoreService> _logger;

        public EventStoreService(EventStoreSettings settings, ILogger<EventStoreService> logger = null)
        {
            _settings = settings;

            _connectionSettings = ConnectionSettings.Create();
            _connectionSettings.EnableVerboseLogging()
                .UseDebugLogger()
                .UseConsoleLogger()
                .KeepReconnecting()
                .DisableServerCertificateValidation()
                .DisableTls()
                .LimitAttemptsForOperationTo(3)
                .LimitRetriesForOperationTo(3)
                .SetHeartbeatTimeout(TimeSpan.FromSeconds(3600))
                .SetHeartbeatInterval(TimeSpan.FromSeconds(3600))
                .WithConnectionTimeoutOf(TimeSpan.FromSeconds(3600))
                .Build();

            _logger = logger;
        }

        public async Task SetStreamMaxAgeAsync(string stream, uint maxAgeInDays)
        {
            _logger?.LogInformation($"Connecting to event store server: {_settings.ServerName}, Tcp port: {_settings.TcpPort}.");

            using (var connection = EventStoreConnection.Create(
                $"ConnectTo=tcp://{_settings.ServerName}:{_settings.TcpPort};DefaultUserCredentials={_settings.AdminUId}:{_settings.AdminPwd};",
                _connectionSettings))
            {
                await connection.ConnectAsync();

                try
                {
                    var maxAgeTimeSpan = TimeSpan.FromDays(maxAgeInDays);

                    var eventStream = await connection.GetStreamMetadataAsync(stream);

                    if (eventStream.StreamMetadata.MaxAge.HasValue && eventStream.StreamMetadata.MaxAge == maxAgeTimeSpan)
                        return;

                    _logger?.LogInformation($"Settting max age ({maxAgeInDays} days) meta data for stream {stream}.");

                    await connection.SetStreamMetadataAsync(stream, eventStream.MetastreamVersion, StreamMetadata.Build().SetMaxAge(maxAgeTimeSpan));

                    _logger?.LogInformation($"Finished settting max age ({maxAgeInDays} days) meta data for stream {stream}.");
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        //public async Task CreateStreamNamesProjectionAsync(string projectionName, string streamsPrefix)
        //{
        //    _logger?.LogInformation($"Connecting to event store server: {_settings.ServerName}, Http port: {_settings.HttpPort}.");

        //    var projectionsManager = new ProjectionsManager(
        //                                            log: new ConsoleLogger(),
        //                                            httpEndPoint: new IPEndPoint(IPAddress.Parse($"{_settings.ServerName}"), (int)_settings.HttpPort),
        //                                            operationTimeout: TimeSpan.FromMinutes(5),
        //                                            httpSchema: "http"                                                   
        //                                            );

        //    var strStreamNameProjection = $@"
        //    fromAll()
        //    .when({{
        //    $init: function(){{
        //            return {{
        //                names: []
        //            }}
        //        }},
        //    $any: function(state, event){{
        //            var streamName = event.streamId;
        //            var r = state.names.find(function(n) {{ return n == streamName; }});
        //            if(typeof r === 'undefined') {{ 
        //                if(streamName.startsWith(""{streamsPrefix}""))
        //                    state.names.push(streamName); 
        //            }}
        //        }}
        //    }})";

        //    var userCredentials = new UserCredentials($"{_settings.AdminUId}", $"{_settings.AdminPwd}");

        //    var projections = await projectionsManager.ListContinuousAsync(userCredentials);

        //    //If projection exists, update the query. Else create projection.
        //    if(projections.Any(p => p.Name == projectionName))
        //    {
        //        _logger?.LogInformation($"Updating projection {projectionName}.");

        //        _logger?.LogInformation($"New projection query: {strStreamNameProjection}");

        //        var currentQuery = await projectionsManager.GetQueryAsync(projectionName, userCredentials);

        //        _logger?.LogInformation($"Is update needed: {strStreamNameProjection != currentQuery}.");

        //        if (strStreamNameProjection != currentQuery)
        //        {
        //            await projectionsManager.UpdateQueryAsync(projectionName, strStreamNameProjection, userCredentials);                    
        //        }

        //        _logger?.LogInformation($"Finished updating projection {projectionName}.");
        //    }
        //    else
        //    {
        //        _logger?.LogInformation($"Creating projection {projectionName}.");

        //        _logger?.LogInformation($"New projection query: {strStreamNameProjection}");

        //        await projectionsManager.CreateContinuousAsync(projectionName, strStreamNameProjection, userCredentials);

        //        _logger?.LogInformation($"Finished creating projection {projectionName}.");
        //    }                        
        //}

        public async Task CreateStreamNamesProjectionAsync(string projectionName, string streamsPrefix)
        {
            _logger?.LogInformation($"Connecting to event store server: {_settings.ServerName}, Http port: {_settings.HttpPort}.");

            var projectionsManager = new ProjectionsManager(
                                                    log: new ConsoleLogger(),
                                                    httpEndPoint: new IPEndPoint(IPAddress.Parse($"{_settings.ServerName}"), (int)_settings.HttpPort),
                                                    operationTimeout: TimeSpan.FromMinutes(5),
                                                    httpSchema: "http"
                                                    );

            var strStreamNameProjection = $@"
            fromAll()
            .when({{
            $init: function(){{
                    return {{
                        names: []
                    }}
                }},
            $any: function(state, event){{
                    var streamName = event.streamId;
                    if(streamName.startsWith(""{streamsPrefix}"")) {{
                        var r = state.names.find(function(n) {{ return n == streamName; }});
                        if(typeof r === 'undefined') {{ 
                            state.names.push(streamName); 
                        }}
                    }}
                    
                }}
            }})";

            var userCredentials = new UserCredentials($"{_settings.AdminUId}", $"{_settings.AdminPwd}");

            var projections = await projectionsManager.ListContinuousAsync(userCredentials);

            //If projection exists, update the query. Else create projection.
            if (projections.Any(p => p.Name == projectionName))
            {
                _logger?.LogInformation($"Updating projection {projectionName}.");

                _logger?.LogInformation($"New projection query: {strStreamNameProjection}");

                var currentQuery = await projectionsManager.GetQueryAsync(projectionName, userCredentials);

                _logger?.LogInformation($"Is update needed: {strStreamNameProjection != currentQuery}.");

                if (strStreamNameProjection != currentQuery)
                {
                    await projectionsManager.UpdateQueryAsync(projectionName, strStreamNameProjection, userCredentials);
                }

                _logger?.LogInformation($"Finished updating projection {projectionName}.");
            }
            else
            {
                _logger?.LogInformation($"Creating projection {projectionName}.");

                _logger?.LogInformation($"New projection query: {strStreamNameProjection}");

                await projectionsManager.CreateContinuousAsync(projectionName, strStreamNameProjection, userCredentials);

                _logger?.LogInformation($"Finished creating projection {projectionName}.");
            }
        }

        public async Task<IEnumerable<string>> GetStreamNamesFromProjectionAsync(string projectionName)
        {
            _logger?.LogInformation($"Connecting to event store server: {_settings.ServerName}, Http port: {_settings.HttpPort}.");

            var projectionsManager = new ProjectionsManager(
                                         log: new ConsoleLogger(),
                                         httpEndPoint: new IPEndPoint(IPAddress.Parse($"{_settings.ServerName}"), (int)_settings.HttpPort),
                                         operationTimeout: TimeSpan.FromMinutes(5),
                                         httpSchema: "http"
                                         );

            _logger?.LogInformation($"Getting state of projection ({projectionName}).");

            var state = await projectionsManager.GetStateAsync(projectionName, new UserCredentials($"{_settings.AdminUId}", $"{_settings.AdminPwd}"));

            _logger?.LogInformation($"Finished getting state of projection ({projectionName}).");

            var names = JObject.Parse(state)["names"];

            return JsonConvert.DeserializeObject<List<string>>(names.ToString());
        }

        public async Task ScavengeAsync()
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(String.Empty);

                _logger?.LogInformation($"Triggering scavenge on event store server ({_settings.ServerName}) on http port {_settings.HttpPort}.");

                await client.PostAsync($"http://{_settings.ServerName}:{_settings.HttpPort}/admin/scavenge", content);

                _logger?.LogInformation("Finished triggering scavenge on event store.");
            }
        }        
    }
}
