﻿namespace EventStore.Maintenance.Services
{
    public class EventStoreSettings
    {
        public string ServerName { get; set; } = "127.0.0.1";
        public uint TcpPort { get; set; } = 1113;
        public uint HttpPort { get; set; } = 2113;
        public string AdminUId { get; set; } = "admin";
        public string AdminPwd { get; set; } = "changeit";
    }
}
