﻿
namespace EventStore.Maintenance.App
{
    public interface IMaintenanceService
    {
        Task CreateStreamNamesProjectionAsync();
        Task WriteStreamNamesToFileAsync();
        Task SetMaxAgeOnStreamsAsync();
    }
}