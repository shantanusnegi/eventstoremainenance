﻿using EventStore.Maintenance.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EventStore.Maintenance.App
{
    public static class Extensions
    {
        public static void AddApp(this IServiceCollection services, IConfiguration config)
        {
            var configSettings = new ConfigSettings
            {
                MaxAgeInDays = uint.Parse(config["MaxAgeInDays"]),
                StreamsPrefix = config["StreamsPrefix"],
                ProjectionName = config["ProjectionName"],
                OutputFile = config["OutputFile"],
                MaxDegreeOfParallelism = int.Parse(config["MaxDegreeOfParallelism"])
            };

            var eventStoreSettings = new EventStoreSettings
            {
                AdminUId = config["EventStore:AdminUId"],
                AdminPwd = config["EventStore:AdminPwd"],
                ServerName = config["EventStore:ServerName"],
                HttpPort = uint.Parse(config["EventStore:HttpPort"]),
                TcpPort = uint.Parse(config["EventStore:TcpPort"])
            };

            services.AddSingleton(configSettings);
            services.AddSingleton(eventStoreSettings);
            services.AddSingleton<IEventStoreService, EventStoreService>();
            services.AddScoped<IMaintenanceService, MaintenanceService>();
        }
    }
}
