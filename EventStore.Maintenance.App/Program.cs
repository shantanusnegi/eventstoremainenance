﻿// See https://aka.ms/new-console-template for more information
using CommandLine;
using EventStore.Maintenance.App;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Configuration;
using Serilog.Core;

Console.WriteLine("Hello, World!");

var hosting = Hosting.CreateWebHostBuilder(args);


//hosting.UseSerilog((x, y) => y.ReadFrom.Configuration(x.Configuration));

hosting.UseSerilog((x, y) => y.WriteTo.Console().WriteTo.Async(a => a.File(x.Configuration["LogFilePath"]), int.MaxValue));

var host = hosting.Build();

var result = await CommandLine.Parser.Default.ParseArguments<CommandlineOptions>(args)
    .WithNotParsed(x => HandleParseError(x, host))
    .WithParsedAsync(x => RunOptions(x, host));

Console.WriteLine("Press any key to exit.");
Console.ReadLine();

static async Task RunOptions(CommandlineOptions opts, IHost host)
{
    //handle options
    var service = (IMaintenanceService)host.Services.GetService(typeof(IMaintenanceService));
    var logger = (ILogger<Program>)host.Services.GetService(typeof(ILogger<Program>));

    try
    {
        logger.LogInformation("Run started.");

        logger.LogInformation($"Commandline options: --CreateProjection/-c {opts.CreateProjection}, --WriteStreamNamesToFile/-w {opts.WriteStreamNamesToFile}, --SetMaxAgeOnStreams/-s {opts.SetMaxAgeOnStreams}");

        if (opts.CreateProjection)
        {
            logger.LogInformation("Processing --CreateProjection/-c true.");

            await service.CreateStreamNamesProjectionAsync();

            logger.LogInformation("Finished processing --CreateProjection/-c true.");
        }            

        if (opts.WriteStreamNamesToFile)
        {
            logger.LogInformation("Processing --WriteStreamNamesToFile/-w true.");

            await service.WriteStreamNamesToFileAsync();

            logger.LogInformation("Finished processing --WriteStreamNamesToFile/-w true.");
        }            

        if (opts.SetMaxAgeOnStreams)
        {
            logger.LogInformation("Processing --SetMaxAgeOnStreams/-s true.");

            await service.SetMaxAgeOnStreamsAsync();

            logger.LogInformation("Finished processing --SetMaxAgeOnStreams/-s true.");
        }

        logger.LogInformation("Run finished.");
    }
    catch (Exception ex)
    {
        logger.LogError(ex, "Error executing run.");
    }
}

static void HandleParseError(IEnumerable<Error> errs, IHost host)
{
    //handle errors
    var logger = (ILogger<Program>)host.Services.GetService(typeof(ILogger<Program>));

    errs.ToList().ForEach(err =>
    {
        logger.LogError(err.ToString());
        Console.WriteLine(err.ToString());
    });
}