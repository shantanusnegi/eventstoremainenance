﻿using CommandLine;

namespace EventStore.Maintenance.App
{
    internal class CommandlineOptions
    {
        [Option('c', "CreateProjection", Required = false, HelpText = "Create the stream names projection in event store.")]
        public bool CreateProjection { get; set; }

        [Option('w', "WriteStreamNamesToFile", Required = false, HelpText = "Write the file containing the stream names from the projection in event store.")]
        public bool WriteStreamNamesToFile { get; set; }

        [Option('s', "SetMaxAgeOnStreams", Required = false, HelpText = "Set the max age on the streams (in the file) in event store.")]
        public bool SetMaxAgeOnStreams { get; set; }
    }
}
