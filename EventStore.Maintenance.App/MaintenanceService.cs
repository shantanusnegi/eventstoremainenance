﻿using EventStore.Maintenance.Services;
using Microsoft.Extensions.Logging;

namespace EventStore.Maintenance.App
{
    public class MaintenanceService : IMaintenanceService
    {
        private readonly ILogger<MaintenanceService> _logger;
        private readonly ConfigSettings _settings;
        private readonly IEventStoreService _eventStoreService;

        public MaintenanceService(ConfigSettings settings, IEventStoreService eventStoreService, ILogger<MaintenanceService> logger = null)
        {
            _logger = logger;
            _settings = settings;
            _eventStoreService = eventStoreService;
        }

        public async Task WriteStreamNamesToFileAsync()
        {
            _logger?.LogInformation("Writing stream names.");

            try
            {
                _logger?.LogInformation($"Reading stream names from projection ({_settings.ProjectionName}) in event store.");

                var streamNames = await _eventStoreService.GetStreamNamesFromProjectionAsync(_settings.ProjectionName);

                _logger?.LogInformation($"Writing stream names to file {_settings.OutputFile}. Total: {streamNames.Count()}");

                using (var file = File.CreateText(_settings.OutputFile))
                {
                    var fileText = string.Join(Environment.NewLine, streamNames);

                    _logger?.LogInformation($"Writing file {_settings.OutputFile}.");

                    file.Write(fileText);                    

                    file.Close();

                    _logger?.LogInformation($"Finished writing file {_settings.OutputFile}.");
                }

                _logger?.LogInformation("Finished writing stream names.");
            }
            catch (Exception ex)
            {
                _logger?.LogError(ex, "Error writing stream names.");
            }
        }

        public async Task SetMaxAgeOnStreamsAsync()
        {
            _logger?.LogInformation("Setting max age on streams.");

            try
            {
                _logger?.LogInformation($"Reading stream names from {_settings.OutputFile}.");

                using (var file = File.OpenRead(_settings.OutputFile))
                {
                    using (var streamReader = new StreamReader(file))
                    {                        
                        var streamNames = await streamReader.ReadToEndAsync();
                        var streamNamesList = streamNames.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);

                        var options = new ParallelOptions
                        {
                            MaxDegreeOfParallelism = _settings.MaxDegreeOfParallelism
                        };

                        await Parallel.ForEachAsync(streamNamesList, options, async (name, ctk) =>
                        {
                            try
                            {
                                _logger?.LogInformation($"Setting max age of stream {name}, max age in days: {_settings.MaxAgeInDays}.");

                                await _eventStoreService.SetStreamMaxAgeAsync(name.Trim(), _settings.MaxAgeInDays);

                                _logger?.LogInformation($"Finished setting max age of stream {name}, max age in days: {_settings.MaxAgeInDays}.");
                            }
                            catch (Exception ex)
                            {
                                _logger?.LogError(ex, $"Error setting max age of stream {name}, max age in days: {_settings.MaxAgeInDays}.");
                            }
                        });

                    }
                }

                _logger?.LogInformation("Finished setting max age on streams.");
            }
            catch (Exception ex)
            {
                _logger?.LogError(ex, "Error setting max age on streams.");
            }
        }

        public async Task CreateStreamNamesProjectionAsync()
        {
            _logger?.LogInformation($"Creating stream names projection ({_settings.ProjectionName}) with streams prefix of {_settings.StreamsPrefix}, in event store.");

            try
            {
                await _eventStoreService.CreateStreamNamesProjectionAsync(_settings.ProjectionName, _settings.StreamsPrefix);

                _logger?.LogInformation("Finished creating stream names projection in event store.");
            }
            catch (Exception ex)
            {
                _logger?.LogError(ex, "Error creating stream names projection in event store.");
            }
        }
    }
}
