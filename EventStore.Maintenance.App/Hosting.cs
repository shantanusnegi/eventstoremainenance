﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.Diagnostics;

namespace EventStore.Maintenance.App
{
    internal class Hosting
    {
		public static IHostBuilder CreateWebHostBuilder(string[] args) =>
		Host.CreateDefaultBuilder(args)
			.UseContentRoot(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName))			
            .ConfigureAppConfiguration((hostingContext, config) =>
            {
                config.AddJsonFile("appsettings.json");
            })
            .ConfigureServices((hostContext, services) =>
            {
				services.AddApp(hostContext.Configuration);
            });
	}
}
