﻿namespace EventStore.Maintenance.App
{
    public class ConfigSettings
    {
        public uint MaxAgeInDays { get; set; }
        public string StreamsPrefix { get; set; }
        public string ProjectionName { get; set; }
        public string OutputFile { get; set; }
        public int MaxDegreeOfParallelism { get; set; }
    }
}
