﻿namespace EventStore.Maintenance
{
    public class ConfigSettings
    {
        public string CronExpression { get; set; }
        public TimeZoneInfo TimeZoneInfo { get; set; }
        public uint MaxAgeInDays { get; set; }
        public string ProjectionName { get; set; }
        public int MaxDegreeOfParallelism { get; set; }
    }
}
