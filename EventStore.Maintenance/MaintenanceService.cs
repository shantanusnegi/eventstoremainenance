﻿using EventStore.Maintenance.Services;
using Microsoft.Extensions.Logging;

namespace EventStore.Maintenance
{
    public class MaintenanceService : CronJobService
    {
        private readonly ILogger<MaintenanceService> _logger;
        private readonly ConfigSettings _settings;
        private readonly IEventStoreService _eventStoreService;

        public MaintenanceService(ConfigSettings settings, IEventStoreService eventStoreService, ILogger<MaintenanceService> logger)
            : base(settings.CronExpression, settings.TimeZoneInfo)
        {
            _logger = logger;
            _settings = settings;
            _eventStoreService = eventStoreService;
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Maintenance Service is starting.");

            await base.StartAsync(cancellationToken);

            _logger.LogInformation("Maintenance Service is started.");
        }

        public override async Task DoWork(CancellationToken cancellationToken)
        {
            //try
            //{
            //    _logger.LogInformation("Setting max age on the streams in the Event Store server.");

            //    _logger.LogInformation($"Getting stream names from the projection ({_settings.ProjectionName}).");

            //    var streams = await _eventStoreService.GetStreamNamesFromProjectionAsync(_settings.ProjectionName);

            //    var options = new ParallelOptions
            //    {
            //        MaxDegreeOfParallelism = _settings.MaxDegreeOfParallelism
            //    };

            //    await Parallel.ForEachAsync(streams, options, async (stream, ctk) =>
            //    {
            //        try
            //        {
            //            _logger.LogInformation($"Setting stream ({stream}) max age ({_settings.MaxAgeInDays} days).");

            //            await _eventStoreService.SetStreamMaxAgeAsync(stream, _settings.MaxAgeInDays);
            //        }
            //        catch(Exception ex)
            //        {
            //            _logger.LogError(ex, $"Error setting stream ({stream}) max age ({_settings.MaxAgeInDays} days).");
            //        }                    
            //    });

            //    _logger.LogInformation("Finished setting max age on the streams in the Event Store server.");
            //}
            //catch (Exception ex)
            //{
            //    _logger.LogError(ex, "Error setting max age on the streams in the Event Store server.");
            //}

            try
            {                
                _logger.LogInformation($"Start triggering scavenge on the Event Store server.");

                await _eventStoreService.ScavengeAsync();

                _logger.LogInformation($"End triggering scavenge on the Event Store server.");
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Error triggering scavenge on the Event Store server.");
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Maintenance Service is stopping.");
            
            await base.StopAsync(cancellationToken);

            _logger.LogInformation("Maintenance Service is stopped.");
        }
    }
}
