﻿using EventStore.Maintenance.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.WindowsServices;

namespace EventStore.Maintenance
{
    public static class Extensions
    {
        public static IServiceCollection AddEventStoreMaintenance(this IServiceCollection services, IConfiguration config)             
        {
            var configSettings = new ConfigSettings
            {
                CronExpression = config["CronExpression"],
                TimeZoneInfo = TimeZoneInfo.Local,
                MaxAgeInDays = uint.Parse(config["MaxAgeInDays"]),
                ProjectionName = config["ProjectionName"],
                MaxDegreeOfParallelism = int.Parse(config["MaxDegreeOfParallelism"])
            };

            var eventStoreSettings = new EventStoreSettings
            {
                AdminUId = config["EventStore:AdminUId"],
                AdminPwd = config["EventStore:AdminPwd"],
                ServerName = config["EventStore:ServerName"],
                HttpPort = uint.Parse(config["EventStore:HttpPort"]),
                TcpPort = uint.Parse(config["EventStore:TcpPort"])
            };

            if (string.IsNullOrWhiteSpace(configSettings.CronExpression))
            {
                throw new ArgumentNullException(nameof(ConfigSettings.CronExpression), @"Empty Cron Expression is not allowed.");
            }

            services.AddSingleton(configSettings);
            services.AddSingleton(eventStoreSettings);
            services.AddSingleton<IEventStoreService, EventStoreService>();

            services.AddSingleton<IHostLifetime, WindowsServiceLifetime>();
            services.AddHostedService<MaintenanceService>();

            return services;
        }
    }
}
