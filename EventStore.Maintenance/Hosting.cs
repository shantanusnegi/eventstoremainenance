﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.EventLog;
using System.Diagnostics;

namespace EventStore.Maintenance
{
    internal class Hosting
    {
		public static IHostBuilder CreateWebHostBuilder(string[] args) =>
		Host.CreateDefaultBuilder(args)
			.UseContentRoot(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName))
			.ConfigureLogging((hostingContext, logging) =>
			{
				logging.ClearProviders();
				logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
				logging.AddEventLog(new EventLogSettings()
				{
					SourceName = "Event Store Maintenance Windows Service",
					LogName = "Application",
					Filter = (x, y) => y >= LogLevel.Information
				});
				logging.AddConsole();
			})
			.ConfigureAppConfiguration((hostingContext, config) =>
			{
				config.AddJsonFile("appsettings.json");
			})
			.ConfigureServices((hostContext, services) =>
			{
				services.AddEventStoreMaintenance(hostContext.Configuration);
			})
            .UseWindowsService();
	}
}
