using EventStore.Maintenance.Services;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace EventStore.Maintenance.Tests
{
    public class EventStoreServiceTests
    {
        [Fact]
        public async Task DeleteEventsPastRetention_Success()
        {
            string stream = "my-stream";
            var configSettings = new ConfigSettings();
            var noOfEvents = 10;

            var service = new EventStoreService(new EventStoreSettings());

            //await service.CreateStreamNamesProjectionAsync("stream-names-projection-6", "my");
           // var streams = await service.GetStreamNamesFromProjectionAsync("stream-names-projection-6");
            //await service.RemoveProjectionAsync("stream-names-projection-5");

            //await service.SetStreamMaxAgeAsync(stream, 10);

            var initialEvents = await EventStoreHelpers.GetStreamEventsAsync(stream, configSettings);

            await EventStoreHelpers.PopulateDummyEvents(stream, 0, noOfEvents, configSettings);

            var addedEvents = await EventStoreHelpers.GetStreamEventsAsync(stream, configSettings);

            Assert.Equal(noOfEvents, addedEvents.Count());

            Thread.Sleep(20000);

            await EventStoreHelpers.PopulateDummyEvents(stream, 0, noOfEvents, configSettings);

            var addedEvents1 = await EventStoreHelpers.GetStreamEventsAsync(stream, configSettings);

            Assert.Equal(noOfEvents, addedEvents1.Count());

            await service.ScavengeAsync();

            var afterScavengeEvents = await EventStoreHelpers.GetStreamEventsAsync(stream, configSettings);

            Assert.Equal(noOfEvents, afterScavengeEvents.Count());
        }
    }
}