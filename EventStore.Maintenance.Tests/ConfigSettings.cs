﻿namespace EventStore.Maintenance.Tests
{
    public class ConfigSettings
    {
        public uint MaxAgeInDays { get; set; } = 10;
        public string Stream { get; set; } = "my-stream";
        public string AdminUId { get; set; } = "admin";
        public string AdminPwd { get; set; } = "changeit";
        public string ServerName { get; set; } = "127.0.0.1";
        public uint TcpPort { get; set; } = 1113; 
    }
}
