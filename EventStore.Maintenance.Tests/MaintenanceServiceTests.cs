﻿using EventStore.Maintenance.App;
using EventStore.Maintenance.Services;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EventStore.Maintenance.Tests
{
    public class MaintenanceServiceTests
    {
        private readonly App.ConfigSettings _configSettings;

        public MaintenanceServiceTests()
        {
            var config = new ConfigurationManager()
                                .AddJsonFile("appsettings.json")
                                .Build();

            _configSettings = new App.ConfigSettings
            {
                MaxAgeInDays = uint.Parse(config["MaxAgeInDays"]),
                StreamsPrefix = config["StreamsPrefix"],
                ProjectionName = config["ProjectionName"],
                OutputFile = config["OutputFile"],
                MaxDegreeOfParallelism = int.Parse(config["MaxDegreeOfParallelism"])
            };
        }

        [Fact]
        public async Task WriteStreamNamesToFileAsync_Success()
        {
            //Arrange
            var eventStoreServiceMock = new Mock<IEventStoreService>();

            IEnumerable<string> streamNames = new List<string> { "my-stream", "my-stream-1" };

            eventStoreServiceMock.Setup(s => s.GetStreamNamesFromProjectionAsync(It.IsAny<string>()))
                                 .Returns(Task.FromResult(streamNames));

            var service = new MaintenanceService(_configSettings, eventStoreServiceMock.Object);

            //Act
            await service.WriteStreamNamesToFileAsync();

            var strStreamNames = File.ReadAllText(_configSettings.OutputFile);

            var streamNamesList = strStreamNames.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);

            //Assert
            Assert.Equal("my-stream", streamNamesList.First());
            Assert.Equal("my-stream-1", streamNamesList.Last());
        }

        [Fact]
        public async Task SetMaxAgeOnStreamsAsync_Success()
        {
            //Arrange
            var eventStoreServiceMock = new Mock<IEventStoreService>();

            eventStoreServiceMock.Setup(s => s.SetStreamMaxAgeAsync(It.IsAny<string>(), It.IsAny<uint>()))
                                 .Returns(Task.CompletedTask);

            IEnumerable<string> streamNames = new List<string> { "my-stream", "my-stream-1" };

            eventStoreServiceMock.Setup(s => s.GetStreamNamesFromProjectionAsync(It.IsAny<string>()))
                                 .Returns(Task.FromResult(streamNames));

            var service = new MaintenanceService(_configSettings, eventStoreServiceMock.Object);

            await service.WriteStreamNamesToFileAsync();

            //Act
            await service.SetMaxAgeOnStreamsAsync();

            //Assert
            eventStoreServiceMock.Verify(m => m.SetStreamMaxAgeAsync("my-stream", _configSettings.MaxAgeInDays), Times.Once());
            eventStoreServiceMock.Verify(m => m.SetStreamMaxAgeAsync("my-stream-1", _configSettings.MaxAgeInDays), Times.Once());
            eventStoreServiceMock.Verify(m => m.SetStreamMaxAgeAsync(It.IsAny<string>(), _configSettings.MaxAgeInDays), Times.Exactly(2));
        }
    }
}
