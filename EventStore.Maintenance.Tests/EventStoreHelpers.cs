﻿using EventStore.ClientAPI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace EventStore.Maintenance.Tests
{
    public class CustomEventData
    {
        public int A { get; set; }
    }

    internal static class EventStoreHelpers
    {
        private static ConnectionSettingsBuilder GetConnectionSettings()
        {
            var connectionSettings = ConnectionSettings.Create();
            connectionSettings.EnableVerboseLogging()
                .UseDebugLogger()
                .UseConsoleLogger()
                .KeepReconnecting()
                .DisableServerCertificateValidation()
                .DisableTls()
                .LimitAttemptsForOperationTo(3)
                .LimitRetriesForOperationTo(3)
                .SetHeartbeatTimeout(TimeSpan.FromSeconds(3600))
                .SetHeartbeatInterval(TimeSpan.FromSeconds(3600))
                .WithConnectionTimeoutOf(TimeSpan.FromSeconds(3600))
                .Build();

            return connectionSettings;
        }

        public static async Task<IEnumerable<ResolvedEvent>> GetStreamEventsAsync(string stream, ConfigSettings settings)
        {
            using (var connection = EventStoreConnection.Create(
                            $"ConnectTo=tcp://{settings.ServerName}:{settings.TcpPort};DefaultUserCredentials={settings.AdminUId}:{settings.AdminPwd};",
                            GetConnectionSettings()))
            {
                await connection.ConnectAsync();

                try
                {
                    return await connection.ReadStreamAsync(stream);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public static async Task PopulateDummyEvents(string stream, int runNoStart, int runNoEnd, ConfigSettings settings)
        {
            using (var connection = EventStoreConnection.Create(
                $"ConnectTo=tcp://{settings.ServerName}:{settings.TcpPort};DefaultUserCredentials={settings.AdminUId}:{settings.AdminPwd};",
                GetConnectionSettings()))
            {
                await connection.ConnectAsync();                

                try
                {
                    for (int i=runNoStart; i<runNoEnd; i++)
                    {
                        var cData = new CustomEventData
                        {
                            A = i
                        };

                        var strData = JsonSerializer.Serialize(cData);

                        var data = Encoding.UTF8.GetBytes(strData);
                        var metadata = Encoding.UTF8.GetBytes("{}");
                        var evt = new EventData(Guid.NewGuid(), "testEvent", true, data, metadata);


                        var result = await connection.AppendToStreamAsync(stream, ExpectedVersion.Any, evt);
                    }                    
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}
