﻿using EventStore.ClientAPI;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventStore.Maintenance.Tests
{
    public static class EventStoreExtensions
    {
        public static async Task<IEnumerable<ResolvedEvent>> ReadStreamAsync(this IEventStoreConnection connection, string streamName)
        {
            var resolvedEvents = new List<ResolvedEvent>();
            StreamEventsSlice currentSlice;

            var nextSliceStart = StreamPosition.Start;
            do
            {
                currentSlice = await connection.ReadStreamEventsForwardAsync(
                    streamName,
                    nextSliceStart,
                    200, false
                );

                nextSliceStart = (int)currentSlice.NextEventNumber;

                resolvedEvents.AddRange(currentSlice.Events);
            } while (!currentSlice.IsEndOfStream);

            return resolvedEvents;
        }
    }
}
