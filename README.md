# Event Store - Delete past retention events

To delete past retention events in the event store,

we set the Max Age on the streams in the event store.

And, we run [scanvenge](https://developers.eventstore.com/server/v21.10/operations.html#scavenging-events), periodically on the event store server.

## Tech stack:

* .Net Core 6
* EventStore.Client 21.2.2
* Cronos 0.7.1
* CommandLineParser 2.8.0
* Serilog 2.10.0
* Moq 4.16.1
* xUnit 2.4.1

The **console app**

* creates a projection to get all the stream names from the Event Store.
* writes all the stream names to a file.
* sets the Max Age on the streams by modifying the stream meta data.

The **windows service**

* triggers the scavenge periodically on the Event Store server.

The app & the windows service use the Event Store Service for interacting with the Event Store.

![Event Store Service](/Docs/EventStoreService.jpg)
